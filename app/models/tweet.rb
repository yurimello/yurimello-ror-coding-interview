# == Schema Information
#
# Table name: tweets
#
#  id         :integer          not null, primary key
#  body       :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#
# Foreign Keys
#
#  user_id  (user_id => users.id)
#
class Tweet < ApplicationRecord
  belongs_to :user
  validates :body, length: {maximum: 180}
  validate :tweet_within_24_hours

  private 
  def tweet_within_24_hours
    errors.add(:body, 'This tweet was created within 24 hours') if Tweet.where(created_at: 1.day.ago..DateTime.now, user_id: user_id, body: body).any?
  end
end
